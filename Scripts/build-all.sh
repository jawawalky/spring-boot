#!/bin/sh

# All Projects
# ============
#
# Builds all Maven projects.
#

echo ">>> All Projects ..."

export TARGET_DIR="../Projects"

./build-project.sh "$TARGET_DIR/Utilities"
./build-project.sh "$TARGET_DIR/ArtemisMQ"
./build-group.sh "$TARGET_DIR/Trail/demos"
./build-group.sh "$TARGET_DIR/Trail/exercises"
./build-group.sh "$TARGET_DIR/JMS/demos"
./build-group.sh "$TARGET_DIR/JMS/exercises"

echo "<<< All Projects."

