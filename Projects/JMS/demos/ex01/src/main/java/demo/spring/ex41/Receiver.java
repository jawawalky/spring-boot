/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex41;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * <h1>The Receiver</h1>
 * 
 * @author Franz Tost
 *
 */
@Component
public class Receiver {
	
	// methods /////

	@JmsListener(destination = "demo") // <- Denotes this method as a consumer
	public void onMessage(             //    of JMS messages sent through
		final String message           //    the queue 'demo'.
	) {
		
		System.out.println("Received message > '" + message + "'");
		
	}

}
