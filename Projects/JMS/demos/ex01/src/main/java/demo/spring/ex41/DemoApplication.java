/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.ex41;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;

/**
 * <h1>The Application</h1>
 * 
 * @author Franz Tost
 *
 */
@SpringBootApplication
@EnableJms                             // <- Enables JMS support and provides
public class DemoApplication           //    several beans, such as
	implements CommandLineRunner       //    the 'JmsTemplate'.
{
	
	// fields /////
	
	@Autowired
	private JmsTemplate jmsTemplate;   // <- Can be autowired, since it was
	                                   //    automatically created by
	                                   //    Spring Boot.
	

	// methods /////

	/**
	 * Runs the demo application.
	 * 
	 * @param args no arguments needed.
	 */
	public static void main(String[] args) {
		
		SpringApplication.run(DemoApplication.class, args);
		
	}

	@Override
	public void run(final String... args) throws Exception {
		
		// <- Here the message is actually sent, by using the 'JmsTemplate'.
		
		this.jmsTemplate.send(
			"demo",
			session -> session.createTextMessage("Hello World")
		);
		
	}

}
