/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.boot.ex06.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.spring.boot.ex06.dao.TranslationRepository;
import demo.spring.boot.ex06.model.Translation;

/**
 * <h1>Services</h1>
 * 
 * Services are <i>Spring</i> beans that usually provide business logic.
 * 
 * @author Franz Tost
 *
 */
@Service
public class TranslationService {
	
	// fields /////
	
	@Autowired
	private TranslationRepository translationRepository;
	
	
	// methods /////
	
	public String translate(final String wordInEnglish) {
			
		final Translation translation =
			this.translationRepository.getByWord(wordInEnglish);
		
		return (translation != null) ? translation.getTranslatedWord() : "---";
		
	}
	
}