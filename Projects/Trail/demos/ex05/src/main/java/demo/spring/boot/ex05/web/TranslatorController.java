/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.boot.ex05.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import demo.spring.boot.ex05.model.Translation;
import demo.spring.boot.ex05.service.TranslationService;

/**
 * <h1>Spring MVC Controllers</h1>
 * 
 * <h2>Controller Class</h2>
 * 
 * A controller is the logic behind a Web page, be it *HTML*, *JSP* etc.
 * 
 * A controller for a JSP page.
 * 
 * <h3>HTTP Request from Browser</h3>
 * 
 * Open a WebBrowser and enter
 * <pre>
 * 	<code>
 * http://localhost:8080/translator/ui
 * 	</code>
 * </pre>
 * The server will respond with <i>Regen</i>.
 * 
 * @author Franz Tost
 *
 */
@Controller
@RequestMapping("/translator")
public class TranslatorController {
	
	// fields /////
	
	@Autowired
	private TranslationService translationService;
	
	
	// methods /////
	
	@GetMapping("/ui")
    public String uiShow(final Model model) {
		
		// <- Adds a 'Translation' model attribute, if there is none.
		
		if (!model.containsAttribute("translation")) {
			
			model.addAttribute("translation", new Translation());
			
		} // if
		
        return "translator";
        
    }

    @PostMapping("/translate")
    public String uiTranslate(
    	@ModelAttribute("translation")
    	final Translation        translation,
    	final RedirectAttributes redirectAttributes
    ) {
    
    	translation.setTranslatedWord(
    		this.translationService.translate(translation.getWord())
    	);
    	
        redirectAttributes.addFlashAttribute("translation", translation);
        
        return "redirect:/translator/ui";
        
    }
    
}
