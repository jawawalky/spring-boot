/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.boot.ex05.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.spring.boot.ex05.service.TranslationService;

/**
 * <h1>Restful WebServices</h1>
 * 
 * <h2>Controller Class</h2>
 * 
 * A controller, which translates English words into German words.
 * We can call the service from a WebBrowser or with an HTTP client.
 * 
 * <h3>HTTP Request from Browser</h3>
 * 
 * Open a WebBrowser and enter
 * <pre>
 * 	<code>
 * http://localhost:8080/translate/rain
 * 	</code>
 * </pre>
 * The server will respond with <i>Regen</i>.
 * 
 * <h3>HTTP Request with curl</h3>
 * 
 * Open a terminal window and enter
 * <pre>
 * 	<code>
 * curl --request GET http://localhost:8080/translate/rain
 * 	</code>
 * </pre>
 * or simply
 * <pre>
 * 	<code>
 * curl http://localhost:8080/translate/rain
 * 	</code>
 * </pre>
 * since the <i>HTTP GET</i> method is the default.
 * 
 * @author Franz Tost
 *
 */
@RestController
@RequestMapping("/translate")
public class TranslatorWebService {
	
	// fields /////
	
	@Autowired
	private TranslationService translationService;
	
	
	// methods /////
	
	@RequestMapping("/{wordInEnglish}")
	public String translate(
		@PathVariable("wordInEnglish") final String wordInEnglish
	) {
		
		return this.translationService.translate(wordInEnglish);
		
	}

}
