/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.boot.ex05.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * <h1>Entities</h1>
 * 
 * Entities are persistable objects. We can add information, needed for
 * persisting the objects, by annotations. In order for some class to be
 * considered as a persistable type, we need to mark it as an entity type,
 * using the {@code Entity} annotation.
 * 
 * @author Franz Tost
 *
 */
@Entity
public class Translation {
	
	// fields /////
	
	@Id @GeneratedValue
	private Long id;
	
	private String word;
	
	private String translatedWord;
	
	
	// constructors /////
	
	public Translation() { }
	
	public Translation(
		final String word,
		final String translation
	) {
		
		this.word = word;
		this.translatedWord = translation;
		
	}
	
	
	// methods /////
	
	public Long getId() { return this.id; }
	
	public String getWord() { return this.word; }
	public void setWord(String word) { this.word = word; }
	
	public String getTranslatedWord() { return this.translatedWord; }
	public void setTranslatedWord(String translation) { this.translatedWord = translation; }
	
}