<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
    <head>
        <meta
            http-equiv = "Content-Type"
            content    = "text/html; charset=ISO-8859-1"
        >
        <title>Translator</title>
    </head>
    <body>
    	<h1>Translator - Spring MVC</h1>
    	<hr>
    	<i>jukia.com</i> - &copy; 2022. All rights reserved.
    	<p> 
        <form:form
            action         = "/translator/translate"
            method         = "POST"
        	modelAttribute = "translation"
        >
            <form:label path="word">Word: </form:label>
            <form:input path="word" type="text"/>
            <form:label path="translatedWord">Translation: </form:label>
            <form:input path="translatedWord" type="text" readonly="true" />
            <input type="submit" value="Submit" />
        </form:form>
    </body>
</html>
