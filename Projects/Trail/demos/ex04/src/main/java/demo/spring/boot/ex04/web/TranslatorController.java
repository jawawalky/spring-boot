/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.boot.ex04.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import demo.spring.boot.ex04.dao.TranslationRepository;
import demo.spring.boot.ex04.model.Translation;

/**
 * <h1>Spring MVC Controllers</h1>
 * 
 * <h2>Controller Class</h2>
 * 
 * A controller is the logic behind a Web page, be it *HTML*, *JSP* etc.
 * 
 * A controller for a JSP page.
 * 
 * <h3>HTTP Request from Browser</h3>
 * 
 * Open a WebBrowser and enter
 * <pre>
 * 	<code>
 * http://localhost:8080/translator/ui
 * 	</code>
 * </pre>
 * The server will respond with <i>Regen</i>.
 * 
 * @author Franz Tost
 *
 */
@Controller                            // <- Makes this class a Spring MVC
                                       //    controller.
@RequestMapping("/translator")
public class TranslatorController {
	
	// fields /////
	
	@Autowired
	private TranslationRepository translationRepository;
	
	
	// methods /////
	
	private String translate(final String wordInEnglish) {
		
		final Translation translation =
			this.translationRepository.getByWord(wordInEnglish);
		
		return (translation != null) ? translation.getTranslatedWord() : "---";
		
	}

	@GetMapping("/ui")
    public String uiShow(final Model model) {
		
		// <- Adds a 'Translation' model attribute, if there is none.
		
		if (!model.containsAttribute("translation")) {
			
			model.addAttribute("translation", new Translation());
			
		} // if
		
        return "translator";           // <- Causes the 'translator.jsp' to
                                       //    be displayed.
        
    }

    @PostMapping("/translate")
    public String uiTranslate(
    	@ModelAttribute("translation")                // <- Injects a model
    	final Translation        translation,         //    attribute into
    	                                              //    a method parameter.
    	
    	final RedirectAttributes redirectAttributes   // <- Allows setting
    	                                              //    attributes on
    	                                              //    a redirect.
    ) {
    
    	// <- Updates the translation.
    	
    	translation.setTranslatedWord(this.translate(translation.getWord()));
    	
    	// <- Flash attributes are temporary attributes that are transfered
    	//    to the model of the redirected page.
    	
        redirectAttributes.addFlashAttribute("translation", translation);
        
        return "redirect:/translator/ui";   // <- Redirects with a HTTP GET
                                            //    request to '/translator/ui'.
        
    }
    
}
