/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.boot.ex04;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import demo.spring.boot.ex04.dao.TranslationRepository;
import demo.spring.boot.ex04.model.Translation;

/**
 * <h1>Spring Boot</h1>
 * 
 * <h2>Web Applications</h2>
 * 
 * @author Franz Tost
 *
 */
@SpringBootApplication
public class DemoApplication
	extends
		SpringBootServletInitializer
	implements
		CommandLineRunner
{
	
	// fields /////
	
	@Autowired
	private TranslationRepository translationRepository;
	
	
	// methods /////

	@Override
	protected SpringApplicationBuilder configure(
		final SpringApplicationBuilder application
	) {
		
		return application.sources(DemoApplication.class);
		
	}

	@Override
	public void run(final String... args) throws Exception {
		
		this.translationRepository.save(new Translation("rain", "Regen"));
		this.translationRepository.save(new Translation("sun",  "Sonne"));
		this.translationRepository.save(new Translation("wind", "Wind"));
		this.translationRepository.save(new Translation("snow", "Schnee"));
		
	}
 
    public static void main(final String[] args) {
    	
    	SpringApplication.run(DemoApplication.class, args);
    	
    }

}