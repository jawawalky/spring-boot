# Spring Boot Web Application

## How to Create a Web Application?

There is not much to do, to convert our simple *Hello World* application
into a Web application. We will provide a restful WebService, which implements
a translator from English to German.

### Build Configuration

With a few changes in out `pom.xml` file we can transform our *Spring Boot*
application into a *Spring Boot* Web application. We will replace the packaging
by `war` instead of `jar` and we will use some *Spring Boot* starters that
automatically embed a Web container (here *Tomcat*) into the application.

Here is the previous `pom.xml` file from our console application, shortened
for brevity

```
<?xml version="1.0" encoding="UTF-8"?>

<project ... >

    ...
    
    <packaging>jar</packaging>
    
    ...
    
    <dependencies>
    
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>
    
    </dependencies>
    
    ...
    
</project>
```

Now here is the `pom.xml` file of our Web application with the parts that we
modified

```
<?xml version="1.0" encoding="UTF-8"?>

<project ... >

    ...
    
    <packaging>war</packaging>
    
    ...
    
    <dependencies>
    
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
    
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-tomcat</artifactId>
            <scope>provided</scope>
        </dependency>
    
    </dependencies>
    
    ...
    
</project>
```

As you can see, the core starter `spring-boot-starter` was replaced by
`spring-boot-starter-web`. For our *Tomcat* support, we added
`spring-boot-starter-tomcat`.

### Application Configuration

*Spring Boot* needs to install its own context into the context of the Web
application. That can be done with a `SpringBootServletInitializer`. It can
be the base class of our application, it can be defined as a *Spring* bean or
simply added as a separate class derived from `SpringBootServletInitializer`.

Here we chose to make it the base class of our application

```
@SpringBootApplication
public class DemoApplication extends SpringBootServletInitializer {
	
	@Override
	protected SpringApplicationBuilder configure(
		final SpringApplicationBuilder application
	) {
		
		return application.sources(DemoApplication.class);
		
	}

    public static void main(final String[] args) {
    	
    	SpringApplication.run(DemoApplication.class, args);
    	
    }
 
}
```

by overriding the `configure(SpringApplicationBuilder)` method, we can
perform configurations and initializations on our Web application.

## Running the Web Application

### Deploying Application on a WebServer

The artifact built is a *WAR*-file, i.e. a Web application file. Such files
can be deployed on Web containers. So if we have a *Tomcat* WebServer running
somewhere, we could copy the *WAR*-file into its *webapps* folder and *Tomcat*
would run our Web application.

### Running Application with Embedded Web Container

Since our application has a `main(...)` method, we can also start it as
a normal *Java* program. When started, an embedded *Tomcat* Web container will
be launched, into which our Web application will be deployed. We can see that
on the output produced by the program

```
2022-04-07 08:58:21.772 INFO 11167 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer : Tomcat started on port(s): 8080 (http) with context path ''
```

## Restful WebService

Now with our Web application set up, we can easily add Web components, such
as restful WebServices for example. A simple class can become a restful
WebService by adding the annotation `@RestController`.

Furthermore we need to tell the system, which path (= URL) will lead to
this service. That is called a *request mapping*. They can be done that
by adding the annotation `@RequestMapping`. Request mappings can be done
on class-level and/or method level. They simply add up to the total path
mapped.

**Example**

Here is a restful WebService, which translates words from English to German

```
@RestController
@RequestMapping("/translate")
public class TranslatorWebService {
	
	@RequestMapping("/{wordInEnglish}")
	public String translate(
		@PathVariable("wordInEnglish") final String wordInEnglish
	) {
	
		switch (wordInEnglish) {
		case "rain": return "Regen";
		case "sun":  return "Sonne";
		case "wind": return "Wind";
		case "snow": return "Schnee";
		default:     return "---";
		}
		
	}

}
```

Path elements can be used as method parameters. They must be declared in
the path with curly brackets (`{<parameter-name>}`). With the annotation
`@PathVariable` and the parameter name, they can be transfered into the value
of a method parameter.
