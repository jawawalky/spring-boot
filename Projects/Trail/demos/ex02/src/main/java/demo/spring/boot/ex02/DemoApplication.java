/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.boot.ex02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/**
 * <h1>Spring Boot</h1>
 * 
 * <h2>Web Applications</h2>
 * 
 * @author Franz Tost
 *
 */
@SpringBootApplication
public class DemoApplication
	extends
		SpringBootServletInitializer   // <- Causes a Web application context
		                               //    to be created and registers
		                               //    servlets, filters and other Web
		                               //    resources.
{
	
	// methods /////

	@Override
	protected SpringApplicationBuilder configure(     // <- Overridden from
		final SpringApplicationBuilder application    //    the 'SpringBootServletInitializer'.
	) {                                               //    It allows us to
		                                              //    configure our Web
		                                              //    application.
		
		return application.sources(DemoApplication.class);
		
	}

    public static void main(final String[] args) {
    	
    	SpringApplication.run(DemoApplication.class, args);
    	
    }
 
}