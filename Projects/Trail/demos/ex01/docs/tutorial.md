# Spring Boot Application

## What is a Spring Boot Application?

A *Spring Boot* application is a *Java* application, which sets up a *Spring*
context and a set of *Spring* beans depending to the type of application you
are building.

Which beans being set up is influenced by

- libraries found on the class path and
- certain configuration annotations.

In general a *Spring Boot* application requires very little configuration in
the first place. With all the defaults set up, we can start programming
complex applications, such as Web or Enterprise Applications right away.

Since not all default configurations will be ones that we need, we can override
them by our own configurations. So usually we start off with the defaults and
then override them when needed.

## Project Setup

### Maven

First we will look at a typical setup for a *Spring Boot* application.
We will use a *Maven* build, but any other build tool works similar.

Using *Maven* we need to write a `pom.xml` file. Here is the one for
the *Hello World* demo project.

```
<?xml version="1.0" encoding="UTF-8"?>

<project
    xmlns              = "http://maven.apache.org/POM/4.0.0"
    xmlns:xsi          = "http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation = "http://maven.apache.org/POM/4.0.0
                          http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>
    
    <groupId>com.jukia.spring.boot</groupId>
    <artifactId>spring-boot-demo01</artifactId>
    <name>Spring Boot [Trail] (01), Hello World Demo</name>
    <packaging>jar</packaging>
    
    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.6.6</version>
        <relativePath/>
    </parent>
    
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
        <java.version>11</java.version>
    </properties>

    <dependencies>
    
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter</artifactId>
        </dependency>
    
    </dependencies>
    
    <build>
        <plugins>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

</project>
```

*Spring Boot* offers a lot of starters, depending on the type of application
you intend to build. When you define a dependency to one or more starters,
then this has an effect on the *Spring* beans bound in the *Spring* context.
If you add a starter for a Web application, then *Spring Boot* automatically
adds the `DispatcherServlet` and a Web container, such as *Tomcat*, to your
application. If you add a database starter, then a default `DataSource` and
a transaction manager will be installed by default.

Since this application should be a simple console application, we only added
the common `spring-boot-starter` dependency.

```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter</artifactId>
</dependency>
```

> You will see other starters in later projects.

We can add the *Spring Boot Maven Plugin* to our build

```
<build>
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
        </plugin>
    </plugins>
</build>
```

It allows us to build and run the application. For more information about
the plugin read the [reference](https://docs.spring.io/spring-boot/docs/current/maven-plugin/reference/htmlsingle/).

### spring initializr

*Spring* provides a service called
[spring initializr](https://start.spring.io/), which allows you to specify
the requirements of your application. You can also specify

- the build tool, either *Maven* or *Gradle*,
- the programming language, either *Java*, *Kotlin* or *Groovy* and
- the *Spring Boot* version.

With all these informations, *spring initializr* creates a project skeleton
with all required dependencies. You can download as a *ZIP*-file and start
programming.

## Implementing Application

Setting up a *Spring Boot* application is pretty simple. Here is an example

```
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication                                          // (1)
public class DemoApplication {
	
    public static void main(final String[] args) {
    	
    	SpringApplication.run(DemoApplication.class, args);     // (2)
    	
    }
 
}
```

1. The annotation `@SpringBootApplication` marks the application as
a *Spring Boot* application. It is an umbrella annotation for several other
annotations, which triggers the auto-configuration of the application.

1. With `SpringApplication.run(DemoApplication.class, args)` *Spring Boot*
starts the auto-discovery of beans, sets up the context and auto-wires
dependencies.

### Console Application

If we want to implement a simple console application, then we can use
the `CommandLineRunner` interface. It offers the method `run(String... args)`,
which serves as the starting point of our application code. So if we add
the `CommandLineRunner` interface to our application class,
then our application looks like this

    	                                         
```
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {
	
    public static void main(final String[] args) {
    	
    	SpringApplication.run(DemoApplication.class, args);
    	
    }
 
    @Override
    public void run(final String... args) {
    	
    	for (String arg : args) {
    		
    		System.out.println("  > " + arg);
    		
    	} // for
    	
    }
    
}
```

The `run(String... args)` method will be called after the *Spring Boot*
application has been initialized, i.e. the context has been set up and beans
have been auto-wired.

# Appendix A - Spring Boot Starters

As said, *Spring Boot* ships with a lot of starters, which help building
applications with certain features quickly. You can find a list of starters
in the [Spring Boot Reference](https://docs.spring.io/spring-boot/docs/2.6.6/reference/htmlsingle/#using.build-systems.starters).

Here are some of the commonly used starters

| Starter | Description |
|:-------:|:-----------:|
| `spring-boot-starter` | A core starter with auto-configuration, logging and *YAML* support. |
| `spring-boot-starter-aop` | A starter for *Spring AOP* and *AspectJ*. |
| `spring-boot-starter-batch` | A stater for *Spring Batch* applications. |
| `spring-boot-starter-data-jdbc` | A starter for *JDBC*-based *Spring Data* applications. |
| `spring-boot-starter-data-jpa` | A starter for *JPA*-based *Spring Data* applications. |
| `spring-boot-starter-data-mongodb` | A starter for *Spring Data* applications using the *MongoDB* database. |
| `spring-boot-starter-data-rest` | A starter for restful *Spring Data* applications. |
| `spring-boot-starter-jdbc` | A starter for *JDBC*-based applications. |
| `spring-boot-starter-web` | A starter for WEB applications. |
| `spring-boot-starter-mail` | A starter for applications, which send mails. |
