/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.boot.ex01;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <h1>Spring Boot</h1>
 * 
 * <h2>Console Applications</h2>
 * 
 * A console-based <i>Spring Boot</i> application.
 * <p>
 * <b>Note</b>
 * <blockquote>
 * 	You can run this program in the <i>Spring Tool Suite</i> by right-clicking
 * 	the {@code DemoApplication} class in the <i>Package Explorer</i> with
 * 	<i>Run As &gt; Spring Boot App</i>.
 * </blockquote>
 * 
 * @author Franz Tost
 *
 */
@SpringBootApplication                 // <- Marks the application as
                                       //    a Spring Boot application and
                                       //    configures some default beans
                                       //    depending on the kind of
                                       //    application that has been chosen,
                                       //    e.g. console application,
                                       //    Web application etc.
public class DemoApplication
	implements CommandLineRunner       // <- Provides an entry point for
	                                   //    an application that can take
	                                   //    some command line arguments.
{
	
	// methods /////

    public static void main(final String[] args) {
    	
    	SpringApplication                        // <- Runs the Spring Boot
    		.run(DemoApplication.class, args);   //    application by setting
    	                                         //    up the Spring context,
    	                                         //    auto-configuring beans
    	                                         //    and autowiring them.
    	
    }
 
    @Override
    public void run(final String... args) {      // <- Entry point for console
    	                                         //    application. Command line
    	                                         //    arguments specified at
    	                                         //    the start of the program
    	                                         //    are passed to
    	                                         //    the 'run(...)' method.
    	
    	System.out.println("Starting application ...");
    	
    	System.out.println("... with the following arguments");
    	
    	for (String arg : args) {
    		
    		System.out.println("  > " + arg);
    		
    	} // for
    	
    	System.out.println("Finished.");
    	
    }
    
}