# Spring Data

## Database Access with Spring Data

*Spring Data* gives us a unified way to access any kind of persistent
data store. That can be a classical, relational database, a document-based
database, a no-SQL database etc.

### Build Configuration

As with Web support, also support for persistence can be added by adding
the appropriate starter library.

For brevity only the relevant entries for persistence have been added to
the example `pom.xml` file.

```
<?xml version="1.0" encoding="UTF-8"?>

<project ... >

    ...
    
    <packaging>jar</packaging>
    
    ...
    
    <dependencies>
    
        ...
    
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-jpa</artifactId>
        </dependency>

        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <scope>runtime</scope>
        </dependency>

    </dependencies>
    
    ...
    
</project>
```

Using *Spring Data* we need to choose the desired persistence technology,
such as *JDBC*, *JPA*, *MongoDB* etc. *Spring Boot* offers adequate starters
for all of those technologies. All those starters start with
`spring-boot-starter-data-xxx`, where `xxx` has to be replaced by
the persistence technology. In this case it is *JPA*.

A database has to be configured. Often just adding some database library
to the build , is sufficient to make *Spring Boot* initialize and start up
some embedded database server of connect to some running server. Here we
added a dependency to the *H2* database, which triggers an embedded, in-memory
database server. Furthermore *JPA* will be configured un such way that is
automatically uses the started database.
