/*
 ******************************************************************************
 *                                                                            *
 *                      J u k i a   S o f t w a r e                           *
 *                      ===========================                           *
 *                                                                            *
 ******************************************************************************
 *                                                                            *
 *          Copyright 2022 - Jukia Software, All Rights Reserved.             *
 *                                                                            *
 *                                                                            *
 *      This software is the proprietary information of Jukia Software.       *
 *                    Use is subject to license terms.                        *
 *                                                                            *
 ******************************************************************************
 */
package demo.spring.boot.ex03.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import demo.spring.boot.ex03.model.Translation;

/**
 * <h1>Repositories</h1>
 * 
 * A repository represents an access point to data stored in some kind of
 * database. Here we store the data in a relational database and use
 * an object-relational-mapping tool to persist Java objects without using
 * SQL commands directly.
 * 
 * @author Franz Tost
 *
 */
public interface TranslationRepository      // <- By deriving an interface from
	extends                                 //    'JpaRepository' it becomes
		JpaRepository<Translation, Long>    //    a repository class, which
                                            //    offers CRUD operations for
                                            //    entities.
{
	
	// methods /////
	
	Translation getByWord(String word);     // <- Finds a translation by
	                                        //    the word that should be
	                                        //    translated.
	
}